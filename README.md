# KGB Mole Detector

This app is meant to detect fake reviews from the KGB on dealerrater.com by scraping 5 pages of the most positive reviews that have been left.

## Set Up

### Must have Node.js version 8.11.2 (https://nodejs.org/en/download/)
### Must have npm version 5.6.0 (https://www.npmjs.com/get-npm)


## To Run Scraper

`npm i`
`npm run scrape`

### then you will see an output similar to this
```
---------------------------------------------------------------------------------------------------
Highest Score: { overallScore: 71,
  reviewTextMetaData: { reviewTextScore: 16 },
  reviewId: 'r4736407',
  employeeReviewMetaData: { employeeReviews: 5, employeeReviewScore: 25 },
  reviewRowsMetaData:
   { 'Customer Service': 5,
     'Quality of Work': 5,
     Friendliness: 5,
     Pricing: 5,
     'Overall Experience': 5,
     'Recommend Dealer': 5 } }
```

## Run Tests
`npm run test`

## Run Linter

`npm run lint`
