const puppeteer = require('./node_modules/puppeteer');
const { flatten } = require('./node_modules/lodash');

// helpers for parsing
const { parseReviewText } = require('./helpers/parseReviewText');
const { parseEmployeeReviews } = require('./helpers/parseEmployeeReviews');
const { parseLabeledReviewRatings } = require('./helpers/parseLabeledReviewRatings');

// pageNumbers of reviews to iterate over
const pageNumbers = [1, 2, 3, 4, 5];

// page content css selectors
const REVIEW_CONTENT_SELECTOR = '.review-content';
const EMPLOYEE_REVIEW_CONTENT_SELECTOR = '.rating-static.pull-left';
const REVIEW_SECTION_CONTENT_SELECTOR = '.review-entry';
const REVIEW_TABLE_CONTENT_SELECTOR = '.review-ratings-all.review-hide';

// url and url params
const BASE_URL = 'https://www.dealerrater.com/dealer/McKaig-Chevrolet-Buick-A-Dealer-For-The-People-dealer-reviews-23685/page';
const URL_PARAMS = '/?filter=ONLY_POSITIVE';

const LOG_SEPERATOR = '---------------------------------------------------------------------------------------------------';

// log final result to console
const logResult = (resultsArray) => {
  const resultLables = ['Highest Score:', 'Second Highest Score:', 'Third Highest Score:'];
  console.log(LOG_SEPERATOR);
  for (let index = 0; index < resultLables.length; index += 1) {
    console.log(resultLables[index], resultsArray[index]);
    console.log(LOG_SEPERATOR);
  }
};

// takes in labeled reviews from review div
// gets the title of the review and the value, and passes those values to parseLabeledReviewRatings
const parseReviewRows = async (reviewRows = []) => {
  let reviewScoreAccumulation = 0;
  const reviewRowsMetaData = {};
  try {
    for (const reviewRow of reviewRows) {
      const labeledReviewRatings = await reviewRow.$$eval('div', nodes => nodes.map(node => ({ class: node.getAttribute('class'), text: node.innerText })));
      const { ratingTitle, score } = parseLabeledReviewRatings(labeledReviewRatings);
      reviewRowsMetaData[ratingTitle] = score;
      reviewScoreAccumulation += score;
    }
  } catch (e) {
    console.log(e);
    return { reviewRowsMetaData: {}, labeledReviewScore: 0 };
  }
  return { reviewRowsMetaData, labeledReviewScore: reviewScoreAccumulation };
};

const parseReviewSections = async (reviewSections = []) => {
  const reviews = [];
  for (const reviewSection of reviewSections) {
    // initialize review object
    const review = { overallScore: 0 };

    // handle text body of review
    const reviewText = await reviewSection.$eval(REVIEW_CONTENT_SELECTOR, node => node.innerText);
    const reviewId = await reviewSection.$$eval('a', nodes => nodes[0].getAttribute('name'));
    const { reviewTextScore, reviewTextMetaData } = parseReviewText(reviewText);
    review.reviewTextMetaData = reviewTextMetaData;
    review.reviewId = reviewId;
    review.overallScore += reviewTextScore;

    // handle employee reviews
    const employeeReviews = await reviewSection.$$eval(EMPLOYEE_REVIEW_CONTENT_SELECTOR, nodes => nodes.map(node => node.getAttribute('class')));
    const { employeeReviewMetaData, employeeReviewScore } = parseEmployeeReviews(employeeReviews);
    review.employeeReviewMetaData = employeeReviewMetaData;
    review.overallScore += employeeReviewScore;

    // handle labeled review ratings
    const reviewBreakdownTable = await reviewSection.$(REVIEW_TABLE_CONTENT_SELECTOR);
    const reviewsRows = await reviewBreakdownTable.$$('.tr');
    const { reviewRowsMetaData, labeledReviewScore } = await parseReviewRows(reviewsRows);
    review.reviewRowsMetaData = reviewRowsMetaData;
    review.overallScore += labeledReviewScore;

    reviews.push(review);
  }
  return reviews;
};

(async () => {
  const browser = await puppeteer.launch();
  // loop through review pages
  const pageReviewResults = await Promise.all(pageNumbers.map(async (pageNumber) => {
    const page = await browser.newPage();
    console.log(`opening review page: ${pageNumber}`);
    await page.goto(`${BASE_URL}${pageNumber}${URL_PARAMS}`);
    const reviewSections = await page.$$(REVIEW_SECTION_CONTENT_SELECTOR);
    return await parseReviewSections(reviewSections);
  }));
  const sortedScores = flatten(pageReviewResults).sort((a, b) => b.overallScore - a.overallScore);
  logResult(sortedScores);
  await browser.close();
})();
