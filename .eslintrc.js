module.exports = {
    "extends": "airbnb-base",
    "rules": {
      "no-console": "off",
      "no-await-in-loop": "off",
      "no-restricted-syntax": "off",
      "no-return-await": "off",
      "radix": "off"
    },
    "globals": {
      "describe": true,
      "it": true
    }
};
