const { expect } = require('./node_modules/chai');

// helpers for parsing
const { parseReviewText } = require('./helpers/parseReviewText');
const { getRatingFromString } = require('./helpers/getRatingFromString');
const { parseEmployeeReviews } = require('./helpers/parseEmployeeReviews');
const { parseLabeledReviewRatings } = require('./helpers/parseLabeledReviewRatings');

describe('KGB Mole Detector', () => {
  describe('getRatingFromString, (Function)', () => {
    it('parses a rating string', () => {
      expect(getRatingFromString('rating-50 test something-else')).to.equal(50);
    });

    it('returns 0 if passed an undefined method', () => {
      expect(getRatingFromString()).to.equal(0);
    });

    it('returns 0 if passed a string with no matching regex', () => {
      expect(getRatingFromString('test something-else')).to.equal(0);
    });
  });

  describe('parseEmployeeReviews, (Function)', () => {
    const employeeReviews = [
      'rating-50 test things stuff KGB-spy',
      'rating-40 test things stuff not-a-KGB-spy',
      'rating-50 test things stuff not-a-KGB-spy',
    ];
    it('returns an accumlation of scores when an array of strings', () => {
      const result = {
        employeeReviewMetaData: {
          employeeReviews: 3,
          employeeReviewScore: 14,
        },
        employeeReviewScore: 14,
      };
      expect(parseEmployeeReviews(employeeReviews)).to.eql(result);
    });

    it('returns an object with default values if no employeeReviews are passed', () => {
      const result = {
        employeeReviewMetaData: {
          employeeReviews: 0,
          employeeReviewScore: 0,
        },
        employeeReviewScore: 0,
      };
      expect(parseEmployeeReviews([])).to.eql(result);
    });

    it('returns an object with default values if no undefined is passed', () => {
      const result = {
        employeeReviewMetaData: {
          employeeReviews: 0,
          employeeReviewScore: 0,
        },
        employeeReviewScore: 0,
      };
      expect(parseEmployeeReviews()).to.eql(result);
    });
  });

  describe('parseLabeledReviewRatings, (Function)', () => {
    it('parses labeledReviewRatings and turns them into an object', () => {
      const labeledReviewRatings = [
        { text: 'Speed', class: 'testing-class' },
        { text: '', class: 'rating-40' },
      ];
      const result = { ratingTitle: 'Speed', score: 4 };
      expect(parseLabeledReviewRatings(labeledReviewRatings)).to.eql(result);
    });

    it('parses labeledReviewRatings returns score: 5 is score text is yes', () => {
      const labeledReviewRatings = [
        { text: 'Would Reccomend To Others', class: 'testing-class' },
        { text: '        Yes\n         ', class: 'nothing-here' },
      ];
      const result = { ratingTitle: 'Would Reccomend To Others', score: 5 };
      expect(parseLabeledReviewRatings(labeledReviewRatings)).to.eql(result);
    });

    it('parses labeledReviewRatings returns score: 0 is score text is no', () => {
      const labeledReviewRatings = [
        { text: 'Would Reccomend To Others', class: 'testing-class' },
        { text: '        No\n         ', class: 'nothing-here' },
      ];
      const result = { ratingTitle: 'Would Reccomend To Others', score: 0 };
      expect(parseLabeledReviewRatings(labeledReviewRatings)).to.eql(result);
    });

    it('returns an empyt object if nothing is passed', () => {
      const result = { ratingTitle: '', score: 0 };
      expect(parseLabeledReviewRatings()).to.eql(result);
    });
  });

  describe('parseReviewText, (Function)', () => {
    it('parses review text and turns it into an object with a score', () => {
      const reviewText = 'Dennis Smith and the team at McKaig did a great job in helping find a vehicle for my son going off to college. It was one of the easiest and best experiences I’ve had at an auto dealership! I pass by there on my way to work and figured I would give them a try. I’m glad I did, as the service Dennis and the team provide is Awesome!!';
      const result = { reviewTextMetaData: { reviewTextScore: 15 }, reviewTextScore: 15 };
      expect(parseReviewText(reviewText)).to.eql(result);
    });

    it('parses review text and turns it into an object with a score even if no arg is passed', () => {
      const result = { reviewTextMetaData: { reviewTextScore: 0 }, reviewTextScore: 0 };
      expect(parseReviewText()).to.eql(result);
    });
  });
});
