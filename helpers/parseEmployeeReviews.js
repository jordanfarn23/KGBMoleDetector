const { getRatingFromString } = require('./getRatingFromString');

// this method takes in class strings from employee review elements on the DOM
// and turns them into numeric scores.
// IE employeeReviews = ['rating-40 some-other-class', 'rating-30'] => 70
// if no elements are found, score is zero
// returns an object with metadata and a score to add to the overall review score

const parseEmployeeReviews = (employeeReviews = []) => {
  let reviewScoreAccumulation = 0;
  for (const employeeReview of employeeReviews) {
    const score = getRatingFromString(employeeReview);
    reviewScoreAccumulation += score;
  }
  const employeeReviewScore = (reviewScoreAccumulation / 10);
  return {
    employeeReviewMetaData: {
      employeeReviews: employeeReviews.length,
      employeeReviewScore,
    },
    employeeReviewScore,
  };
};

module.exports = { parseEmployeeReviews };
