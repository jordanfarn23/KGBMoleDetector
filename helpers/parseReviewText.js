const Sentiment = require('../node_modules/sentiment');

// this function takes in a reviewText param and analyzes it for sentiment
// returns an object with metadata and a score to add to the overall review score

// create reviewText sentiment analyzer
const sentiment = new Sentiment();

const parseReviewText = (reviewText = '') => {
  const reviewTextScore = sentiment.analyze(reviewText).score;
  return { reviewTextMetaData: { reviewTextScore }, reviewTextScore };
};

module.exports = { parseReviewText };
