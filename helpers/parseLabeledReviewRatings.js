const { getRatingFromString } = require('./getRatingFromString');

// this takes two parsed dom elements and turns them into a numeric value
// ratingTitle is the title of the rating that the customer gave a review of
// ratingScore is the 1-5 star value given to that rating title
// IE Customer Support: **** (4/5 stars) would return a score of 4
// returns an object with metadata and a score to add to the overall review score

const parseLabeledReviewRatings = (labeledReviewRatings = []) => {
  const [ratingTitle = { class: '', text: '' }, ratingScore = { text: '' }] = labeledReviewRatings;
  let parsedReviewScore;
  if (ratingScore.text.length) {
    parsedReviewScore = ratingScore.text.replace(/\s/g, '').toLowerCase() === 'yes' ? 50 : 0;
  } else {
    parsedReviewScore = getRatingFromString(ratingScore.class);
  }
  return { ratingTitle: ratingTitle.text, score: (parsedReviewScore / 10) };
};

module.exports = { parseLabeledReviewRatings };
