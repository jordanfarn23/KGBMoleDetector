const { get } = require('../node_modules/lodash');

// visiual ratings are scraped by a class, example: getRatingFromString('rating-50') => 50
// function returns an object with metadata and a score to add to the overall review score

const getRatingFromString = (string = '') => {
  const regexedString = string.match(/rating-([0-9]+)/);
  return parseInt(get(regexedString, '[1]', 0));
};

module.exports = { getRatingFromString };
